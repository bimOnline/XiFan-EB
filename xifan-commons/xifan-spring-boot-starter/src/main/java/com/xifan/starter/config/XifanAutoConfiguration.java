package com.xifan.starter.config;

import com.xifan.starter.propertis.XifanSystemProperties;
import com.xifan.starter.service.IXifanSystemService;
import com.xifan.starter.service.impl.IXifanSystemServiceImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/5/31 0031
 */

@Configuration
@ConditionalOnClass(IXifanSystemService.class)
@EnableConfigurationProperties(XifanSystemProperties.class)
public class XifanAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public IXifanSystemService xifanSystemService() {
        return new IXifanSystemServiceImpl();
    }
}
