package com.xifan.starter.propertis;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/5/31 0031
 */

@Getter
@Setter
@ConfigurationProperties("xifan.system")
public class XifanSystemProperties {

    /**
     * 系统负责人
     */
    private String master = "Default";

    /**
     * 负责人联系邮箱
     */
    private String email = "rundreams@yeah.net";


}
