package com.xifan.starter.service;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/5/31 0031
 */
public interface IXifanSystemService {

    /**
     * 打印系统配置信息
     */
    void printSystemInfo();
}
