package com.xifan.starter.service.impl;

import com.xifan.starter.propertis.XifanSystemProperties;
import com.xifan.starter.service.IXifanSystemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/5/31 0031
 */

@Slf4j
public class IXifanSystemServiceImpl implements IXifanSystemService {

    @Autowired
    XifanSystemProperties xifanSystemProperties;

    @Override
    public void printSystemInfo() {
        log.debug("当前系统负责人MASTER={}", xifanSystemProperties.getMaster());
        log.debug("当前系统负责人EMAIL={}", xifanSystemProperties.getEmail());
    }
}
