package com.xifan.commons.cores.constant;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/15 0015
 */
public class FacadeConstants {

    /**
     * gid长度.
     */
    public static final int GID_LENGTH = 36;
}
