package com.xifan.commons.cores.result;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 * 封装分页查询返回
 *
 * @Author: Ray Allen  @Time:2020/2/20 0020
 */

@Getter
@Setter
public class PageResult<T> implements Serializable {
    private static final long serialVersionUID = 8054970129307664712L;

    private boolean success = false;

    private String message = "查询失败";

    private List pageList = new ArrayList<String>();

    private long totalCount = 0;

    public PageResult() {
    }

    public PageResult(boolean success, String message, List<T> pageList, long totalCount) {
        this.success = success;
        this.message = message;
        this.pageList = pageList;
        this.totalCount = totalCount;
    }

    /**
     * 获取一个分页查询结果为空的结果
     *
     * @return
     */
    public static <T> PageResult<T> emptyResult() {
        return new PageResult<T>(false, "查询结果为空", null, 0);
    }

    /**
     * 获取一个分页查询失败的结果
     *
     * @param msg
     * @param <T>
     * @return
     */
    public static <T> PageResult<T> getFailResult(String msg) {
        return new PageResult<T>(false, msg, null, 0);
    }

    /**
     * 获取一个分页查询成功的结果
     *
     * @return
     */
    public static <T> PageResult<T> getSuccessResult(List pageList, long totalCount) {
        return new PageResult<T>(true, "查询成功", pageList, totalCount);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
