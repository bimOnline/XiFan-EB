package com.xifan.commons.cores.result;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 * 分页查询对象
 *
 * @Author: Ray Allen  @Time:2020/2/20 0020
 */

@Getter
@Setter
public class BasePageListRequestDTO extends AbstractRequestDTO {

    /**
     * key 查询
     */
    private String key;

    /**
     * 当前页
     */
    private long current = 1;

    /**
     * 每页显示条数，默认 10
     */
    private long size = 10;

    /**
     * 是否进行 count 查询
     */
    private boolean whSearchCount = true;

    /**
     * 自定义封装查询参数
     *
     * @param conditions
     */
    public void buildBaseConditions(Map<String, Object> conditions) {
        conditions.put("current", getCurrent());
        conditions.put("size", getSize());
        conditions.put("isPaged", 0);
    }

    @Override
    protected boolean isValidGid() {
        return false;
    }

    @Override
    public void valid() {

    }
}
