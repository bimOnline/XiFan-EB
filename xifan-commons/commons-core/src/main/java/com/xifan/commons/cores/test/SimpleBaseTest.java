package com.xifan.commons.cores.test;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/16 0016
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class SimpleBaseTest extends CoreTest {
}
