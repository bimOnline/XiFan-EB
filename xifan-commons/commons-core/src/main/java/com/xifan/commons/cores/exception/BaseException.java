package com.xifan.commons.cores.exception;

import com.xifan.commons.cores.enums.EnumsResponseCode;

/**
 * 追忆寻梦
 * rundreams.net
 *
 * @Author:zzh rundreams@yeah.net  @Time:2020/2/12 0012
 */

public class BaseException extends RuntimeException {

    /**
     * 请求业务是否成功
     */
    private boolean success;

    /**
     * 返回信息
     */
    private String message;

    /**
     * 应答码
     */
    private EnumsResponseCode responseCode;



}
