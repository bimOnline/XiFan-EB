package com.xifan.commons.util.boot;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.ConfigurableApplicationContext;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/13 0013
 */

@Slf4j
public class BootPrintServiceUrlUtil {

    public static void printServiceUrl(ConfigurableApplicationContext ctx) {
        try {
            String host = InetAddress.getLocalHost().getHostAddress();
            TomcatServletWebServerFactory tomcatServletWebServerFactory = (TomcatServletWebServerFactory) ctx.getBean("tomcatServletWebServerFactory");
            int port = tomcatServletWebServerFactory.getPort();
            String contextPath = tomcatServletWebServerFactory.getContextPath();
            String url = "http://" + host + ":" + port + contextPath + "/";
            log.info("--------------启动成功-------------   " + url);
        } catch (UnknownHostException e) {
            log.error("--------------启动失败-------------");
            log.error("--------------启动失败-------------", e);
            e.printStackTrace();
        }
    }
}
