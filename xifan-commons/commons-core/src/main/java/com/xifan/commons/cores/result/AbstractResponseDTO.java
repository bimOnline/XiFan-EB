package com.xifan.commons.cores.result;

import com.xifan.commons.cores.constant.XifanConstants;
import com.xifan.commons.cores.enums.EnumsResponseCode;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.checkerframework.checker.units.qual.A;

import java.io.Serializable;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/12 0012
 */

@Getter
@Setter
public class AbstractResponseDTO implements Serializable {

    private static final long serialVersionUID = 4940327352509650037L;

    /**
     * 请求业务是否成功
     */
    private boolean success;

    /**
     * 返回信息
     */
    private String message;

    /**
     * 应答码
     */
    private EnumsResponseCode responseCode;

    /**
     * 返回的对象
     */
    public Object object = new Object();


    /**
     * 执行成功 自定义 消息、object对象
     *
     * @param message
     * @param object
     * @return
     */
    public static AbstractResponseDTO success(String message, Object object) {
        AbstractResponseDTO responseDTO = new AbstractResponseDTO();
        responseDTO.setSuccess(true);
        responseDTO.setMessage(XifanConstants.SUCCESS_MESSAGE);
        responseDTO.setResponseCode(EnumsResponseCode.EXECUTE_SUCCESS);
        responseDTO.setObject(null);
        return responseDTO;
    }

    public static AbstractResponseDTO success() {
        return success(XifanConstants.SUCCESS_MESSAGE, null);
    }

    public static AbstractResponseDTO success(Object object) {
        return success(XifanConstants.SUCCESS_MESSAGE, object);
    }

    /**
     * 执行失败 自义定返回
     *
     * @param message
     * @param responseCode
     * @param object
     * @return
     */
    public static AbstractResponseDTO fail(String message, EnumsResponseCode responseCode, Object object) {
        AbstractResponseDTO responseDTO = new AbstractResponseDTO();
        responseDTO.setSuccess(false);
        responseDTO.setMessage(message);
        responseDTO.setResponseCode(responseCode);
        responseDTO.setObject(object);
        return responseDTO;
    }

    public static AbstractResponseDTO fail(String message, EnumsResponseCode responseCode) {
        return fail(message, responseCode, null);
    }

    public static AbstractResponseDTO fail(String message) {
        return fail(message, EnumsResponseCode.BIZ_EXCEPTION, null);
    }

    public static AbstractResponseDTO failCheckParameter() {
        return fail("参数校验失败", EnumsResponseCode.ILLEGAL_ARGUMENT_EXCEPTION, null);
    }

    public static AbstractResponseDTO failNoData() {
        return fail("无满足条件的数据", EnumsResponseCode.NOT_MEET_CONDITIONS_DATA, null);
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
