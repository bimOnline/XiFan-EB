package com.xifan.commons.cores.env;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/13 0013
 */

public class Profiles {

    // 生产环境
    public static final String PRO = "pro";

    // 预发布环境
    public static final String PRE = "pre";

    // 测试环境
    public static final String TEST = "test";

    // 开发环境
    public static final String DEV = "dev";

    // 单元测试 【方便加载相应服务】
    public static final String UNIT_TESTING = "unit_testing";
}
