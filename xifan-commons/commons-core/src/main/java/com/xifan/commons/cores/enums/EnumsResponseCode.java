package com.xifan.commons.cores.enums;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/15 0015
 */

public enum EnumsResponseCode {

    /**
     * 未知异常.
     */
    UNKNOWN_EXCEPTION("UNKNOWN_EXCEPTION", "未知异常"),

    /**
     * 不合法或不正确的参数.
     */
    ILLEGAL_ARGUMENT_EXCEPTION("ILLEGAL_ARGUMENT_EXCEPTION", "不合法或不正确的参数"),

    /**
     * 没有查询到满足条件的数据.
     */
    NOT_MEET_CONDITIONS_DATA("NOT_MEET_CONDITIONS_DATA", "没有查询到满足条件的数据"),

    /**
     * 业务异常.
     */
    BIZ_EXCEPTION("BIZ_EXCEPTION", "业务异常"),

    /**
     * 数据库异常.
     */
    DATABASE_EXCEPTION("DATABASE_EXCEPTION", "数据库异常"),

    /**
     * 幂等性校验不通过.
     */
    IDEMPOTENCE_EXCEPTION("IDEMPOTENCE_EXCEPTION", "幂等性校验不通过"),

    /**
     * 数据已经存在.
     */
    DATA_EXIST_ALREADY("DATA_EXIST_ALREADY", "数据已经存在"),

    /**
     * 执行成功.
     */
    EXECUTE_SUCCESS("EXECUTE_SUCCESS", "执行成功"),

    /**
     * 连接超时.
     */
    CONNECTION_TIMEOUT("CONNECTION_TIMEOUT", "连接超时");

    /**
     * 枚举值
     */
    private final String code;

    /**
     * 枚举描述
     */
    private final String message;

    /**
     * @param code    枚举值
     * @param message 枚举描述
     */
    private EnumsResponseCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * @return Returns the code.
     */
    public String getCode() {
        return code;
    }

    /**
     * @return Returns the message.
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return Returns the code.
     */
    public String code() {
        return code;
    }

    /**
     * @return Returns the message.
     */
    public String message() {
        return message;
    }

    /**
     * 通过枚举<code>code</code>获得枚举
     *
     * @param code
     * @return MemberRoleEnum
     */
    public static EnumsResponseCode getByCode(String code) {
        for (EnumsResponseCode _enum : values()) {
            if (_enum.getCode().equals(code)) {
                return _enum;
            }
        }
        return null;
    }

    /**
     * 获取全部枚举
     *
     * @return List<MemberRoleEnum>
     */
    public static java.util.List<EnumsResponseCode> getAllEnum() {
        java.util.List<EnumsResponseCode> list = new java.util.ArrayList<EnumsResponseCode>(
                values().length);
        for (EnumsResponseCode _enum : values()) {
            list.add(_enum);
        }
        return list;
    }

    /**
     * 通过code获取msg
     *
     * @param code 枚举值
     * @return
     */
    public static String getMsgByCode(String code) {
        if (code == null) {
            return null;
        }
        EnumsResponseCode _enum = getByCode(code);
        if (_enum == null) {
            return null;
        }
        return _enum.getMessage();
    }

    /**
     * 获取枚举code
     *
     * @param _enum
     * @return
     */
    public static String getCode(EnumsResponseCode _enum) {
        if (_enum == null) {
            return null;
        }
        return _enum.getCode();
    }

}
