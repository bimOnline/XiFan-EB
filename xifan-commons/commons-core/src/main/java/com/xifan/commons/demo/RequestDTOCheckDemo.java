package com.xifan.commons.demo;

import com.xifan.commons.cores.constant.XifanConstants;
import com.xifan.commons.cores.result.AbstractRequestDTO;
import com.xifan.commons.cores.validate.group.CreateGroup;
import com.xifan.commons.cores.validate.group.UpdateGroup;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.*;


/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/15 0015
 */

@Setter
@Getter
@Slf4j
public class RequestDTOCheckDemo extends AbstractRequestDTO {

    /**
     * 字符串类型校验
     */
    @NotEmpty(groups = CreateGroup.class)
    @Length(min = 5, max = 10, groups = CreateGroup.class)
    private String name;

    /**
     * 限制数字 输入区间
     */
    @NotNull(groups = CreateGroup.class)
    @Range(groups = CreateGroup.class, min = 1, max = 120, message = "年龄必须为1-120岁")
    private Integer age;

    /**
     * 邮箱不能为空，邮箱格式验证
     */
    @NotEmpty(groups = CreateGroup.class, message = "邮箱不能为空")
    @Email(groups = CreateGroup.class)
    private String email;

    @NotEmpty(groups = UpdateGroup.class, message = "手机号不能为空")
    @Pattern(groups = UpdateGroup.class, regexp = XifanConstants.MOBILE_REGEXP, message = "手机号格式不正确")
    private String phone;

    @Override
    protected boolean isValidGid() {
        return true;
    }

    @Override
    public void valid() {
        validate(this, CreateGroup.class, UpdateGroup.class);
    }


    public static void main(String[] args) {


        RequestDTOCheckDemo demo = new RequestDTOCheckDemo();
        demo.setName("123wewe");
        demo.setAge(1);
        demo.setEmail("1@qq.com");
        demo.setPhone("13274008954");

        try {
            demo.valid();
        } catch (Exception e) {
            log.error("异常", e);
            log.info("返回消息:" + e.getMessage());
        }
    }
}
