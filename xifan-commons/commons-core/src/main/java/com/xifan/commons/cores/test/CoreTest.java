package com.xifan.commons.cores.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/13 0013
 */

public class CoreTest {

    /**
     * 日志
     */
    public final Logger logger = LoggerFactory.getLogger(getClass());
}
