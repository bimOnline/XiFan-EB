package com.xifan.commons.cores.constant;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/13 0013
 */

public class XifanConstants {

    /**
     * dubbo 默认版本 1.0
     */
    public static final String DUBBO_VERSION_DEFAULT = "1.0";

    /**
     * dubbo 版本 2.0
     */
    public static final String DUBBO_VERSION_2 = "2.0";

    /**
     * OPTIONS 请求常量
     */
    public static final String OPTIONS = "OPTIONS";

    /**
     * 系统错误提示
     */
    public static final String ERROR_MESSAGE = "未知异常,请联系客服或技术人员!";

    /**
     * 无权限错误提示
     */
    public static final String NO_AUTHORITY_ERROR_MESSAGE = "你没有该接口权限!";

    /**
     * 无接口权限 错误url
     */
    public static final String ERROR_API_URL = "/error/apiError";


    /**
     * 用户未登录 错误url
     */
    public static final String ERROR_NO_LOGIN_URL = "/error/noLogin";

    /**
     * 执行成功提示
     */
    public static final String SUCCESS_MESSAGE = "执行成功!";

    /**
     * excel导入导出数据条数限制
     */
    public static final Integer EXCEL_DATA_LIMIT = 5000;

    /**
     * 手机正则表达式.
     */
    public static final String MOBILE_REGEXP = "^[1][3,4,5,7,8][0-9]{9}$";

}
