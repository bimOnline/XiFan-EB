package com.xifan.commons.cores.controller;

import cn.hutool.core.util.StrUtil;
import com.xifan.commons.cores.constant.XifanConstants;
import com.xifan.commons.cores.enums.EnumsHttpCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/13 0013
 */

public class AbstractController {

    /**
     * 响应码
     */
    public static final String FLAG = "flag";
    /**
     * 响应信息
     */
    public static final String MSG = "msg";
    /**
     * 响应结果对象
     */
    public static final String DATA = "data";

    /**
     * 响应结果附带对象
     */
    public static final String RESULT = "result";

    /**
     * 日志
     */
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 成功返回 输出
     *
     * @param msg
     * @param result
     * @param data
     * @return
     */
    protected Map<String, Object> successView(String msg, Object result, Object data) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put(FLAG, EnumsHttpCode.OK.getCode());
        resultMap.put(MSG, msg);
        resultMap.put(RESULT, result);
        resultMap.put(DATA, data);
        return resultMap;
    }

    /**
     * result data
     *
     * @param result
     * @param data
     * @return
     */
    protected Map<String, Object> successView(Object result, Object data) {
        return successView(XifanConstants.SUCCESS_MESSAGE, result, data);
    }

    /**
     * data
     *
     * @param data
     * @return
     */
    protected Map<String, Object> successView(Object data) {
        return successView(XifanConstants.SUCCESS_MESSAGE, "", data);
    }

    /**
     * 返回成功
     *
     * @return
     */
    protected Map<String, Object> successView() {
        return successView(XifanConstants.SUCCESS_MESSAGE, "", "");
    }

    /**
     * 失败返回 输出
     *
     * @param msg
     * @param result
     * @param data
     * @return
     */
    protected Map<String, Object> failView(String msg, Object result, Object data) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put(FLAG, EnumsHttpCode.RETURN_FAIL.getCode());
        if (StrUtil.isEmpty(msg)) {
            resultMap.put(MSG, XifanConstants.ERROR_MESSAGE);
        } else {
            resultMap.put(MSG, msg);
        }
        resultMap.put(RESULT, result);
        resultMap.put(DATA, data);
        return resultMap;
    }

    /**
     * 统一输出 失败返回 msg ,通常用于业务异常
     *
     * @param msg
     * @return
     */
    protected Map<String, Object> failView(String msg) {
        return failView(msg, "", "");
    }

    /**
     * 统一输出 失败，通常用于 catch 系统异常等情况
     *
     * @return
     */
    protected Map<String, Object> failView() {
        return failView(XifanConstants.ERROR_MESSAGE);
    }

    /**
     * 自定义api 输出
     *
     * @param code
     * @param msg
     * @param result
     * @param data
     * @return
     */
    protected Map<String, Object> objectView(EnumsHttpCode code, String msg, Object result, Object data) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put(FLAG, code.getCode());
        if (!StrUtil.equals(code.getCode().toString(), EnumsHttpCode.OK.getCode().toString())) {
            if (StrUtil.isEmpty(msg)) {
                resultMap.put(MSG, XifanConstants.ERROR_MESSAGE);
            } else {
                resultMap.put(MSG, msg);
            }
        }
        resultMap.put(RESULT, result);
        resultMap.put(DATA, data);
        return resultMap;
    }
}
