package com.xifan.iversion.dal.mapper.auto;

import com.xifan.iversion.dal.entity.auto.OrderPaymentNotifyDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单支付通知表 Mapper 接口
 * </p>
 *
 * @author 追忆寻梦 Ray Allen
 * rundreams.net
 * rundreams@yeah.net 
 * @since 2020-05-23
 */
public interface OrderPaymentNotifyMapper extends BaseMapper<OrderPaymentNotifyDO> {

}
