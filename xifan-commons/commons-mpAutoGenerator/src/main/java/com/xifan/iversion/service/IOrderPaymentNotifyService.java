package com.xifan.iversion.service;

import com.xifan.iversion.dal.entity.auto.OrderPaymentNotifyDO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单支付通知表 服务类
 * </p>
 *
 * @author 追忆寻梦 Ray Allen
 * rundreams.net
 * rundreams@yeah.net 
 * @since 2020-05-23
 */
public interface IOrderPaymentNotifyService extends IService<OrderPaymentNotifyDO> {

}
