package com.xifan.iversion.service.impl;

import com.xifan.iversion.dal.entity.auto.OrderPaymentNotifyDO;
import com.xifan.iversion.dal.mapper.auto.OrderPaymentNotifyMapper;
import com.xifan.iversion.service.IOrderPaymentNotifyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单支付通知表 服务实现类
 * </p>
 *
 * @author 追忆寻梦 Ray Allen
 * rundreams.net
 * rundreams@yeah.net 
 * @since 2020-05-23
 */
@Service
public class OrderPaymentNotifyServiceImpl extends ServiceImpl<OrderPaymentNotifyMapper, OrderPaymentNotifyDO> implements IOrderPaymentNotifyService {

}
