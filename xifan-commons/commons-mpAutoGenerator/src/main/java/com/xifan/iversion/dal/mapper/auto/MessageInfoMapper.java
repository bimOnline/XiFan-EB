package com.xifan.iversion.dal.mapper.auto;

import com.xifan.iversion.dal.entity.auto.MessageInfoDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 消息表 Mapper 接口
 * </p>
 *
 * @author 追忆寻梦 Ray Allen
 * rundreams.net
 * rundreams@yeah.net 
 * @since 2020-05-23
 */
public interface MessageInfoMapper extends BaseMapper<MessageInfoDO> {

}
