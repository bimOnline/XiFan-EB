package com.xifan.iversion.service;

import com.xifan.iversion.dal.entity.auto.MessageInfoDO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 消息表 服务类
 * </p>
 *
 * @author 追忆寻梦 Ray Allen
 * rundreams.net
 * rundreams@yeah.net 
 * @since 2020-05-23
 */
public interface IMessageInfoService extends IService<MessageInfoDO> {

}
