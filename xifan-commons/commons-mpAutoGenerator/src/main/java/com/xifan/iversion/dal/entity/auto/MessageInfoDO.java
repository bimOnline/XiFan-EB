package com.xifan.iversion.dal.entity.auto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 消息表
 * </p>
 *
 * @author 追忆寻梦 Ray Allen
 * rundreams.net
 * rundreams@yeah.net 
 * @since 2020-05-23
 */
@Data
  @EqualsAndHashCode(callSuper = false)
  @Accessors(chain = true)
@TableName("message_info")
public class MessageInfoDO extends Model<MessageInfoDO> {

    private static final long serialVersionUID=1L;

      @TableId(value = "id", type = IdType.AUTO)
      private Long id;

      /**
     * 消息编号
     */
      private String msgNo;

      /**
     * 消息标题
     */
      private String msgTitle;

      /**
     * 消息内容
     */
      private String msgContent;

      /**
     * 备注
     */
      private String remark;

      /**
     * 备注扩展
     */
      private String remarkExtend;

      /**
     * 创建时间
     */
      private LocalDateTime createTime;

      /**
     * 修改时间
     */
      private LocalDateTime updateTime;

      /**
     * 0未删除 1 已删除
     */
      private Integer deleted;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
