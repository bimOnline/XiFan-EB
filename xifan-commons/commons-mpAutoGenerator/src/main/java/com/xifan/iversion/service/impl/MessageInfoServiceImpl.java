package com.xifan.iversion.service.impl;

import com.xifan.iversion.dal.entity.auto.MessageInfoDO;
import com.xifan.iversion.dal.mapper.auto.MessageInfoMapper;
import com.xifan.iversion.service.IMessageInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 消息表 服务实现类
 * </p>
 *
 * @author 追忆寻梦 Ray Allen
 * rundreams.net
 * rundreams@yeah.net 
 * @since 2020-05-23
 */
@Service
public class MessageInfoServiceImpl extends ServiceImpl<MessageInfoMapper, MessageInfoDO> implements IMessageInfoService {

}
