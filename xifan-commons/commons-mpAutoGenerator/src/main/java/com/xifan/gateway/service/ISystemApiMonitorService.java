package com.xifan.gateway.service;

import com.xifan.gateway.dal.entity.auto.SystemApiMonitorDO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统API监控  服务类
 * </p>
 *
 * @author 追忆寻梦 Ray Allen
 * rundreams.net
 * rundreams@yeah.net 
 * @since 2020-05-23
 */
public interface ISystemApiMonitorService extends IService<SystemApiMonitorDO> {

}
