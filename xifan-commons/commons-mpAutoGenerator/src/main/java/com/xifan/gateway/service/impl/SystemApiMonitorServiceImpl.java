package com.xifan.gateway.service.impl;

import com.xifan.gateway.dal.entity.auto.SystemApiMonitorDO;
import com.xifan.gateway.dal.mapper.auto.SystemApiMonitorMapper;
import com.xifan.gateway.service.ISystemApiMonitorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统API监控  服务实现类
 * </p>
 *
 * @author 追忆寻梦 Ray Allen
 * rundreams.net
 * rundreams@yeah.net 
 * @since 2020-05-23
 */
@Service
public class SystemApiMonitorServiceImpl extends ServiceImpl<SystemApiMonitorMapper, SystemApiMonitorDO> implements ISystemApiMonitorService {

}
