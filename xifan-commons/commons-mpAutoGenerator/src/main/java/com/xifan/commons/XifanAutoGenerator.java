package com.xifan.commons;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.lang.reflect.Field;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/16 0016
 */

public class XifanAutoGenerator {

    // TODO 所属数据库
    private static final String JDBC_DATABASE = "xifan_gateway";

    // TODO 本地mybatis-plus-generator 路径 【 根据需求修改 】
    private static String OUTPUT_DIR = "D:\\rundreams\\gitee\\XiFan-EB\\xifan-commons\\commons-mpAutoGenerator\\src\\main\\java";
    //TODO 每个系统parent 包名 【 根据需求修改 】
    private static String PARENT_PACKAGE = "com.xifan.gateway";
    //TODO 注释模板 【 根据需求修改 】
    private static String AUTHOR = "追忆寻梦 Ray Allen\n" +
            " * rundreams.net\n" +
            " * rundreams@yeah.net ";

    public static void main(String[] args) throws Exception {

        AutoGenerator mpg = new AutoGenerator();

        //TODO 全局相关配置
        mpg.setGlobalConfig(gc());

        //TODO 数据源绑定
        mpg.setDataSource(dsc());

        //TODO 策略配置项
        StrategyConfig strategy = new StrategyConfig();
        // lombok
        strategy.setEntityLombokModel(true);
        // strategy.setRestControllerStyle(true);
        // 表名生成策略
        strategy.setNaming(NamingStrategy.underline_to_camel);
        // 需要生成的表,大小写一定要正确
//        strategy.setInclude(new String[]{"message_info", "order_payment_notify"});
        strategy.setInclude(new String[]{"system_api_monitor"});
        // 排除生成的表
        Field field = strategy.getClass().getDeclaredField("logicDeleteFieldName");
        field.setAccessible(true);
        field.set(strategy, "logic_del");
        mpg.setStrategy(strategy);

        //TODO 包 相关配置
        mpg.setPackageInfo(pc());

        // 执行生成
        mpg.execute();
        System.out.println("自动构建完成！");

    }

    private static GlobalConfig gc() {
        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        gc.setOutputDir(OUTPUT_DIR);
        // 是否覆盖已有文件
        gc.setFileOverride(true);
        // 是否打开输出目录
        gc.setOpen(false);
        // 开发人员
        gc.setAuthor(AUTHOR);
        // 开启 ActiveRecord 模式
        gc.setActiveRecord(true);
        // 开启 BaseResultMap
        gc.setBaseResultMap(true);
        // 开启 baseColumnList
        gc.setBaseColumnList(true);
        // 指定生成的主键的ID类型  雪花算法实现
        gc.setIdType(IdType.ASSIGN_ID);
        // 各层文件名称方式，例如： %sAction 生成 UserAction %s 为占位符
        gc.setEntityName("%sDO");
        gc.setMapperName("%sMapper");
        gc.setXmlName("%sMapper");
//        gc.setServiceName("%sService");
//        gc.setServiceImplName("%sServiceImpl");
//        gc.setControllerName("%sController");
        return gc;
    }

    private static DataSourceConfig dsc() {
        // 数据源配置mysql
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDbType(DbType.MYSQL);
        // mysql 8.0版本 驱动
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUrl("jdbc:mysql://119.3.65.140:3306/" + JDBC_DATABASE + "?useUnicode=true&characterEncoding=UTF8&useSSL=false&nullCatalogMeansCurrent=true");
        dsc.setUsername("root");
        dsc.setPassword("root123");
        return dsc;
    }

    private static PackageConfig pc() {
        // 包配置
        PackageConfig pc = new PackageConfig();
        // 父包名
        pc.setParent(PARENT_PACKAGE);
        // Entity包名
        pc.setEntity("dal.entity.auto");
        // Mapper包名
        pc.setMapper("dal.mapper.auto");
        // Mapper XML包名
        pc.setXml("dal.mapperxml.auto");
        return pc;
    }


}
