package com.xifan.iversion;

import com.xifan.commons.util.boot.BootPrintServiceUrlUtil;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * 追忆寻梦
 * rundreams.net
 *
 * @Author:zzh rundreams@yeah.net  @Time:2020/2/12 0012
 */

@Slf4j
@SpringBootApplication
@MapperScan({"com.xifan.iversion.dal.mapper.auto", "com.xifan.iversion.dal.mapper.extra"})
public class IversionApplication {

    public static void main(String[] args) {
            ConfigurableApplicationContext ctx = SpringApplication.run(IversionApplication.class, args);
            BootPrintServiceUrlUtil.printServiceUrl(ctx);
    }
}
