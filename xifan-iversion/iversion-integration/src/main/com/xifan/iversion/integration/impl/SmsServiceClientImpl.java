package com.xifan.iversion.integration.impl;

import com.xifan.commons.cores.result.ResultBase;
import com.xifan.gateway.alibaba.SmsService;
import com.xifan.iversion.integration.SmsServiceClient;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/13 0013
 */

@Service
public class SmsServiceClientImpl implements SmsServiceClient {

//    @Reference(version = "1.0")
//    SmsService smsService;

    @Override
    public ResultBase getName(String id) {
//        return smsService.getName(id);
        return ResultBase.success();
    }
}
