package com.xifan.iversion.integration;

import com.xifan.commons.cores.result.ResultBase;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/13 0013
 */

public interface SmsServiceClient {


    /**
     * @param id
     * @return
     */
    ResultBase getName(String id);
}
