package com.xifan.iversion.mp;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xifan.iversion.SpringbootBaseTest;
import com.xifan.iversion.dal.entity.auto.MessageInfoDO;
import com.xifan.iversion.dal.mapper.auto.MessageInfoMapper;
import com.xifan.iversion.util.IversionConstants;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/16 0016
 */

public class MybatisPlusTestSpringboot extends SpringbootBaseTest {

    @Autowired
    MessageInfoMapper messageInfoMapper;

    @Test
    public void insert() {
        MessageInfoDO messageInfoDO = new MessageInfoDO();
        messageInfoDO.setMsgNo(IdUtil.simpleUUID());
        messageInfoDO.setMsgTitle("留言");
        messageInfoDO.setMsgContent("留言内容");
        messageInfoDO.setCreateTime(LocalDateTime.now());
        logger.info(messageInfoDO.toString());
        boolean insert = messageInfoDO.insert();
        logger.info("插入是否成功" + insert);

    }

    @Test
    public void query() {
        QueryWrapper<MessageInfoDO> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(MessageInfoDO::getId, 1).eq(MessageInfoDO::getDeleted, 1);
        List<MessageInfoDO> messageInfoDOS = messageInfoMapper.selectList(queryWrapper);
        messageInfoDOS.forEach(System.out::println);
    }

    @Test
    public void delete() {
        MessageInfoDO messageInfoDO = new MessageInfoDO();
        messageInfoDO.setId(1L);
        boolean deleteById = messageInfoDO.deleteById();
        logger.info("插入是否成功" + deleteById);
    }

    @Test
    public void selectByPage() {

        MessageInfoDO messageInfoDO = new MessageInfoDO();

        Page<MessageInfoDO> page = new Page<>(1,3);

        Page<MessageInfoDO> messageInfoDOPage = messageInfoMapper.selectPage(page, Wrappers.emptyWrapper());


        logger.info("查询list返回messageInfoDOPage={}", messageInfoDOPage);

        List<MessageInfoDO> records = messageInfoDOPage.getRecords();
        records.forEach(System.out::println);
    }
}
