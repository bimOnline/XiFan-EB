package com.xifan.iversion;

import com.xifan.commons.cores.env.Profiles;
import com.xifan.commons.cores.test.CoreTest;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 追忆寻梦
 * rundreams.net
 *
 * @Author:zzh rundreams@yeah.net  @Time:2020/2/12 0012
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = com.xifan.iversion.IversionApplication.class)
@ActiveProfiles(value = Profiles.TEST)
public class SpringbootBaseTest extends CoreTest {
}
