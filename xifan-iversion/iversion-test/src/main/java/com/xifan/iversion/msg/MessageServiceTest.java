package com.xifan.iversion.msg;

import cn.hutool.core.date.DateUtil;
import com.xifan.commons.cores.result.AbstractResponseDTO;
import com.xifan.commons.cores.result.PageResult;
import com.xifan.iversion.SpringbootBaseTest;
import com.xifan.iversion.facade.MessageService;
import com.xifan.iversion.facade.parameter.request.MessageInfoRequestDTO;
import com.xifan.iversion.facade.parameter.request.MessageQueryRequestDTO;
import com.xifan.iversion.facade.parameter.response.bo.MessageInfoBO;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/20 0020
 */

public class MessageServiceTest extends SpringbootBaseTest {

    @Autowired
    MessageService messageService;

    @Test
    public void pageList() {

        MessageQueryRequestDTO requestDTO = new MessageQueryRequestDTO();
        requestDTO.setWhSearchCount(false);
        PageResult<MessageInfoBO> messageInfoBOPageResult = messageService.queryMessagePageList(requestDTO);

        logger.info("单元测试返回");
        logger.info("单元测试返回messageInfoBOPageResult={}", messageInfoBOPageResult);

    }

    @Test
    public void insertDemo() {

        MessageInfoRequestDTO requestDTO = new MessageInfoRequestDTO();
        requestDTO.setMsgNo(DateUtil.now());
        requestDTO.setMsgTitle("头部");
        requestDTO.setMsgContent("内容");

        AbstractResponseDTO abstractResponseDTO = messageService.insertDemo(requestDTO);
        logger.info("返回abstractResponseDTO={}", abstractResponseDTO);
    }
}
