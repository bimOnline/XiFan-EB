package com.xifan.iversion.hutool;

import cn.hutool.core.lang.Console;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.net.NetUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RuntimeUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/20 0020
 */
@Slf4j
public class HutoolTest {

    public static void main(String[] args) {
        String str = RuntimeUtil.execForStr("ipconfig");
      log.info(str);

        log.info(NetUtil.localIpv4s().toString());

        //参数1为终端ID
        //参数2为数据中心ID
        Snowflake snowflake = IdUtil.createSnowflake(1, 1);
        long id = snowflake.nextId();
        Console.log(id);

    }
}
