package com.xifan.iversion.facade.parameter.request;

import com.xifan.commons.cores.result.BasePageListRequestDTO;
import lombok.Getter;
import lombok.Setter;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/20 0020
 */

@Getter
@Setter
public class MessageQueryRequestDTO extends BasePageListRequestDTO {

    /**
     * x消息编号
     */
    private String msgNo;
}
