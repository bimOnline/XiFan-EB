package com.xifan.iversion.facade.parameter.request;

import com.xifan.commons.cores.result.AbstractRequestDTO;
import lombok.Getter;
import lombok.Setter;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/20 0020
 */

@Getter
@Setter
public class MessageInfoRequestDTO extends AbstractRequestDTO {

    /**
     * 消息编号
     */
    private String msgNo;

    /**
     * 消息标题
     */
    private String msgTitle;

    /**
     * 消息内容
     */
    private String msgContent;

    /**
     * 备注
     */
    private String remark;

    /**
     * 备注扩展
     */
    private String remarkExtend;


    @Override
    protected boolean isValidGid() {
        return false;
    }

    @Override
    public void valid() {

    }
}
