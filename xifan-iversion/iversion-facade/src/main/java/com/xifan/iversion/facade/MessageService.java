package com.xifan.iversion.facade;

import com.xifan.commons.cores.result.AbstractResponseDTO;
import com.xifan.commons.cores.result.PageResult;
import com.xifan.iversion.facade.parameter.request.MessageInfoRequestDTO;
import com.xifan.iversion.facade.parameter.request.MessageQueryRequestDTO;
import com.xifan.iversion.facade.parameter.response.bo.MessageInfoBO;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/20 0020
 */
public interface MessageService {

    /**
     * 分页查询 消息列表
     *
     * @param requestDTO
     * @return
     */
    PageResult<MessageInfoBO> queryMessagePageList(MessageQueryRequestDTO requestDTO);


    /**
     * 插入多张表 测试分布式事务
     *
     * @param requestDTO
     * @return
     */
    AbstractResponseDTO insertDemo(MessageInfoRequestDTO requestDTO);
}
