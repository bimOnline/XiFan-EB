package com.xifan.iversion.facade.parameter.request;

import com.xifan.commons.cores.result.AbstractRequestDTO;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 订单支付通知表
 * </p>
 *
 * @author 追忆寻梦 Ray Allen
 * rundreams.net
 * rundreams@yeah.net
 * @since 2020-05-23
 */
@Getter
@Setter
public class OrderPaymentNotifyRequestDTO extends AbstractRequestDTO {


    /**
     * 商户订单号
     */
    private String outTradeNo;

    /**
     * 微信支付订单号
     */
    private String transactionId;

    /**
     * 支付完成时间
     */
    private String notifyTime;

    /**
     * 付款银行
     */
    private String bankType;

    /**
     * 现金支付金额
     */
    private String cashFee;

    /**
     * 交易类型JSAPI、NATIVE、APP
     */
    private String tradeType;

    /**
     * 备注
     */
    private String remark;

    /**
     * 备注扩展
     */
    private String remarkExtend;

    @Override
    protected boolean isValidGid() {
        return false;
    }

    @Override
    public void valid() {

    }
}
