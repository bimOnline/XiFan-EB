package com.xifan.iversion.biz.message;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xifan.commons.cores.constant.XifanConstants;
import com.xifan.commons.cores.result.AbstractResponseDTO;
import com.xifan.commons.cores.result.PageResult;
import com.xifan.iversion.dal.entity.auto.MessageInfoDO;
import com.xifan.iversion.dal.mapper.auto.MessageInfoMapper;
import com.xifan.iversion.dal.mapper.auto.OrderPaymentNotifyMapper;
import com.xifan.iversion.facade.MessageService;
import com.xifan.iversion.facade.parameter.request.MessageInfoRequestDTO;
import com.xifan.iversion.facade.parameter.request.MessageQueryRequestDTO;
import com.xifan.iversion.facade.parameter.response.bo.MessageInfoBO;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/20 0020
 */

@Service(version = XifanConstants.DUBBO_VERSION_DEFAULT)
public class MessageServiceImpl implements MessageService {

    @Autowired
    MessageInfoMapper messageInfoMapper;

    @Autowired
    OrderPaymentNotifyMapper orderPaymentNotifyMapper;

    @Override
    public PageResult<MessageInfoBO> queryMessagePageList(MessageQueryRequestDTO requestDTO) {

        Page<MessageInfoDO> page = new Page<>(requestDTO.getCurrent(), requestDTO.getSize(), requestDTO.isWhSearchCount());

        LambdaQueryWrapper<MessageInfoDO> wrapper = Wrappers.<MessageInfoDO>lambdaQuery();
        if (StrUtil.isNotBlank(requestDTO.getMsgNo())) {
            wrapper.eq(MessageInfoDO::getMsgNo, requestDTO.getMsgNo());
        }

        Page<MessageInfoDO> messageInfoDOPage = messageInfoMapper.selectPage(page, wrapper);
        if (messageInfoDOPage.getTotal() < 0) {
            return PageResult.emptyResult();
        }

        List<MessageInfoBO> reList = new ArrayList<>();
        messageInfoDOPage.getRecords().stream().forEach(msg -> {
            MessageInfoBO msgBO = new MessageInfoBO();
            BeanUtils.copyProperties(msg, msgBO);
            reList.add(msgBO);
        });

        return PageResult.getSuccessResult(reList, messageInfoDOPage.getTotal());
    }

    @Override
    public AbstractResponseDTO insertDemo(MessageInfoRequestDTO requestDTO) {

        MessageInfoDO infoDO = new MessageInfoDO();
        BeanUtils.copyProperties(requestDTO, infoDO);
        infoDO.setCreateTime(DateUtil.toLocalDateTime(new Date()));
        messageInfoMapper.insert(infoDO);
        return AbstractResponseDTO.success();
    }
}
