create table `message_info` (
	`id` bigint(20) not null auto_increment,
	`msg_no` varchar(32) null default '' comment '消息编号',
	`msg_title` varchar(32) null default '' comment '消息标题',
	`msg_content` varchar(68) null default '' comment '消息内容',
	`remark` varchar(1024) null default '' comment '备注',
	`remark_extend` varchar(1024) null default '' comment '备注扩展',
	`create_time` timestamp not default current_timestamp comment '创建时间',
	`update_time` timestamp null default current_timestamp on update current_timestamp comment '修改时间',
	`deleted` int(1) null default '0' comment '0未删除 1 已删除',
	primary key (`id`)
)comment='消息表' collate='utf8_general_ci' engine=innodb auto_increment=0;



-- 订单支付通知表
create table `order_payment_notify` (
	`id` int (11) not null auto_increment,
	`out_trade_no` varchar (64) not null comment '商户订单号',
	`transaction_id` varchar(64) not null comment '微信支付订单号',
	`notify_time` varchar(32) not null comment '支付完成时间',
	`bank_type` varchar(32) null default '' comment '付款银行',
	`cash_fee` varchar(32) null default '0' comment '现金支付金额',
	`trade_type` varchar(32) null default '' comment '交易类型JSAPI、NATIVE、APP',
	`remark` varchar (1024) null default null comment '备注',
	`remark_extend` varchar (1024) null default null comment '备注扩展',
	`create_time` datetime not null comment '创建时间',
	`update_time` timestamp null default current_timestamp on update current_timestamp comment '修改时间',
	`is_delete` int (1) null default '0' comment '0未删除 1 已删除',
	primary key (`id`),
  index `out_trade_no_index`(`out_trade_no`),
  index `transaction_id_index`(`transaction_id`)
) comment = '订单支付通知表' collate = 'utf8_general_ci' engine = innodb auto_increment = 0;

