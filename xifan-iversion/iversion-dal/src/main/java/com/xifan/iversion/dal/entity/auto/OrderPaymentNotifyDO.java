package com.xifan.iversion.dal.entity.auto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 订单支付通知表
 * </p>
 *
 * @author 追忆寻梦 Ray Allen
 * rundreams.net
 * rundreams@yeah.net 
 * @since 2020-05-23
 */
@Data
  @EqualsAndHashCode(callSuper = false)
  @Accessors(chain = true)
@TableName("order_payment_notify")
public class OrderPaymentNotifyDO extends Model<OrderPaymentNotifyDO> {

    private static final long serialVersionUID=1L;

      @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

      /**
     * 商户订单号
     */
      private String outTradeNo;

      /**
     * 微信支付订单号
     */
      private String transactionId;

      /**
     * 支付完成时间
     */
      private String notifyTime;

      /**
     * 付款银行
     */
      private String bankType;

      /**
     * 现金支付金额
     */
      private String cashFee;

      /**
     * 交易类型JSAPI、NATIVE、APP
     */
      private String tradeType;

      /**
     * 备注
     */
      private String remark;

      /**
     * 备注扩展
     */
      private String remarkExtend;

      /**
     * 创建时间
     */
      private LocalDateTime createTime;

      /**
     * 修改时间
     */
      private LocalDateTime updateTime;

      /**
     * 0未删除 1 已删除
     */
      private Integer isDelete;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
