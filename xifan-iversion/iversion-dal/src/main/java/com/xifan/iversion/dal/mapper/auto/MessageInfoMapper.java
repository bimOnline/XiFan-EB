package com.xifan.iversion.dal.mapper.auto;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xifan.iversion.dal.entity.auto.MessageInfoDO;

/**
 * <p>
 * 站内消息管理表 Mapper 接口
 * </p>
 *
 * @author 追忆寻梦 Ray Allen
 * rundreams.net
 * rundreams@yeah.net 
 * @since 2020-02-16
 */
public interface MessageInfoMapper extends BaseMapper<MessageInfoDO> {

}
