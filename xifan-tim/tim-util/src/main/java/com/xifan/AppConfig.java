package com.xifan;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen @Time:2020/3/2
 */
@Component
public class AppConfig {

    /**
     * 启动环境
     */
    public static String envValue;

    @Value("${spring.profiles.active}")
    public void setEnvValue(String envValue) {
        this.envValue = envValue;
    }


    //=====================nacos config========================

    public static String httpUrl;

    // 通过nacos配置中心去获取 赋值给静态变量 httpUrl
    // config ex : tim.config.httpUrl=http://www.rundreams.top1
    @NacosValue(value = "${tim.config.httpUrl}", autoRefreshed = true)
    public void setHttpUrl(String httpUrl) {
        this.httpUrl = httpUrl;
    }

}
