package com.xifan.ts;

import com.xifan.BaseTest;
import com.xifan.commons.cores.cache.RedisUtil;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/13 0013
 */

public class TsTest extends BaseTest {

    @Autowired
    RedisUtil redisUtil;

    @Test
    public void redi() {

        redisUtil.set("name","zhangzihao");

        System.out.println( redisUtil.get("name"));

    }
}
