package com.xifan.api;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.xifan.api.base.BaseApi;
import com.xifan.commons.cores.cache.RedisUtil;
import com.xifan.commons.cores.result.PageResult;
import com.xifan.integration.MessageServiceClient;
import com.xifan.iversion.facade.parameter.request.MessageQueryRequestDTO;
import com.xifan.iversion.facade.parameter.response.bo.MessageInfoBO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/20 0020
 */

@RestController
@RequestMapping("/api/msg")
@Api(value = "/api/msg", tags = "消息处理API")
public class MessageApi extends BaseApi {

    @Autowired
    MessageServiceClient messageServiceClient;

    @Autowired
    RedisUtil redisUtil;

    @NacosValue(value = "${userName}", autoRefreshed = true)
    private String userName;

    @ApiOperation(value = "得到userName", httpMethod = "GET", notes = "得到userName")
    @GetMapping("/userName")
    public Object userName() {
        return successView(userName);
    }


    @ApiOperation(value = "Redis", httpMethod = "GET", notes = "Redis")
    @GetMapping("/redis/{key}")
    public Object rediskey(@PathVariable String key, HttpServletRequest request) {

        String s = RandomUtil.randomUUID();
        String s2 = String.valueOf(RandomUtil.randomNumber());
        logger.info("{},{}", s, s2);

        boolean set = redisUtil.set(s, s2);
        return successView(set);
    }


    @GetMapping("/queryMessagePageList/{msgNo}")
    public Object queryMessagePageList(@PathVariable String msgNo, HttpServletRequest request) {

        MessageQueryRequestDTO requestDTO = new MessageQueryRequestDTO();
        requestDTO.setMsgNo(msgNo);
        PageResult<MessageInfoBO> pageResult = messageServiceClient.queryMessagePageList(requestDTO);
        if (pageResult.isSuccess()) {
            return successView(pageResult.getTotalCount(), pageResult.getPageList());
        } else {
            return failView(pageResult.getMessage());
        }
    }

}
