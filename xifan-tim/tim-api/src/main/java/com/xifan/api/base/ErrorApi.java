package com.xifan.api.base;

import cn.hutool.core.util.StrUtil;
import com.xifan.commons.cores.enums.EnumsHttpCode;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen @Time:2020/3/4
 */

@RestController
@RequestMapping(value = "/error")
public class ErrorApi extends BaseApi {

    @RequestMapping(value = "/apiTokenError")
    public Object apiTokenError(String msg) {
        logger.info("apiTokenError msg={}", msg);
        if (StrUtil.isNotBlank(msg)) {
            return objectView(EnumsHttpCode.METHOD_NOT_ALLOWED, msg, null, null);
        }
        return objectView(EnumsHttpCode.METHOD_NOT_ALLOWED, " TOKEN鉴权失败", null, null);
    }
}
