package com.xifan.api.base;

import com.xifan.commons.cores.cache.RedisUtil;
import com.xifan.commons.cores.controller.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/20 0020
 */

public class BaseApi extends AbstractController {

    @Autowired
    protected RedisUtil redisUtil;

}
