package com.xifan.api;

import com.xifan.AppConfig;
import com.xifan.api.base.BaseApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * rundreams.net
 *
 * @Email: rundreams@yeah.net
 * @Author: rundreams.top @Time:2020/3/30
 */

@RestController
@RequestMapping("/api")
@Api(value = "/api", tags = "Nacos测试")
public class NacosApi extends BaseApi {

    /**
     * nacos demo
     * http://127.0.0.1:8911/api/nacos
     *
     * @return
     */
    @GetMapping("/nacos")
    @ApiOperation(value = "获取配置信息", httpMethod = "GET", notes = "获取配置信息")
    public Object nacos() {
        return successView(AppConfig.httpUrl);
    }

}
