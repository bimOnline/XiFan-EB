package com.xifan.config;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * http://127.0.0.1:8911/doc.html
 * rundreams.net by zhangzihao
 *
 * @Author:rundreams@yeah.net @Time:2020/7/22
 */

@Configuration
@EnableSwagger2
@EnableSwaggerBootstrapUI
public class SwaggerConfig {

    @Value("${swagger.enable}")
    private boolean enableSwagger;

    @Bean
    public Docket createRestApi() {
        //http://ip地址:端口/项目名/swagger-ui.html#/
        ApiInfo apiInfo = new ApiInfoBuilder()
                //网站标题
                .title("Xifan-Tim")
                //网站描述
                .description("RUNDREAMS网络科技有限公司")
                //版本
                .version("1.0.0")
                //联系人
                .contact(new Contact("Ray Allen", "http://rundreams.top/", "rundreams@yeah.net"))
                //协议
                .license("The Apache License")
                //协议url
                .licenseUrl("http://rundreams.top/")
                .build();

        //swagger版本
        return new Docket(DocumentationType.SWAGGER_2)
                .pathMapping("/")
                .select()
                //扫描那些controller
                .apis(RequestHandlerSelectors.basePackage("com.xifan.api"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo)
                // 是否开启swagger接口文档
                .enable(enableSwagger);
    }

}
