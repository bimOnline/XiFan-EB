package com.xifan.config;

import cn.hutool.core.util.StrUtil;
import com.xifan.AppConfig;
import com.xifan.commons.cores.env.Profiles;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen @Time:2020/3/2
 */

@Slf4j
@Component
public class AuthInterceptor implements HandlerInterceptor {

    // 改为通过字典进行获取，动态改变 appSecret
    private static final String appSecret = "bxaresg4ceefiqolvmxfztfjulsryzav";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        // 测试环境返回 true
        if (StrUtil.equals(Profiles.DEV, AppConfig.envValue) || StrUtil.equals(Profiles.TEST, AppConfig.envValue)) {
            return true;
        }

        // TODO 参数
        // rand 随机数
        // appSecret 用于接口加密
        // timestamp 时间戳 1583135309740
        // visitorToken 签名字符串= rand + url +
        // url 当前请求url
//        String sendRedirectMsg = "";
//
//        String rand = request.getParameter("rand");
//        Long timestamp = Long.valueOf(request.getParameter("timestamp"));
//        String url = request.getRequestURI();
//        // 签名token
//        String visitorToken = request.getHeader("visitorToken");
//
//        // OPTIONS请求 true
//        if (StrUtil.equals(request.getMethod(), "OPTIONS")) {
//            log.info("NetCourseInterceptor OPTIONS请求过滤...");
//            return true;
//        }
//
//        log.info("rand={},timestamp={},url={},visitorToken={}", rand, timestamp, url, visitorToken);
//
//        // TODO 加密方式 (((rand + url + timestamp) md5) + appSecret) md5
//        // TODO 判断 rand 是否重复
//        // TODO 判断 timestamp 是否过期
//        // TODO 判断 visitorToken 是否相等
//        JedisUtil jedisUtil2 = JedisUtil.getInstance();
//        String key = jedisUtil2.getKey("tim" + rand);
//        if (StrUtil.isNotBlank(key)) {
//            sendRedirectMsg = "TOKEN鉴权失败:请求重放";
//            sendRedirectMsg = URLEncoder.encode(sendRedirectMsg, "UTF-8");
//            response.sendRedirect("/error/apiTokenError?msg=" + sendRedirectMsg);
//            return false;
//        }
//
//        Long time = ((DateTime.now().getTime() - timestamp) / 1000);
//        if (time > 60L) {
//            sendRedirectMsg = "TOKEN鉴权失败:时间重放";
//            sendRedirectMsg = URLEncoder.encode(sendRedirectMsg, "UTF-8");
//            response.sendRedirect("/error/apiTokenError?msg=" + sendRedirectMsg);
//            return false;
//        }
//        StringBuffer sb = new StringBuffer(rand);
//        sb.append(url);
//        sb.append(timestamp);
//        String to = SecureUtil.md5(sb.toString());
//        String token = SecureUtil.md5(to + appSecret);
//        if (!StrUtil.equals(visitorToken, token)) {
//            sendRedirectMsg = "TOKEN鉴权失败:鉴权异常";
//            sendRedirectMsg = URLEncoder.encode(sendRedirectMsg, "UTF-8");
//            response.sendRedirect("/error/apiTokenError?msg=" + sendRedirectMsg);
//            return false;
//        }
//
//        JedisUtil jedisUtil = JedisUtil.getInstance();
//        jedisUtil.setEx("tim" + rand, 60, rand);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }


}
