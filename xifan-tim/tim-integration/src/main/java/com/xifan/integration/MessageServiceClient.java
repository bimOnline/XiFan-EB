package com.xifan.integration;

import com.xifan.commons.cores.result.PageResult;
import com.xifan.iversion.facade.parameter.request.MessageQueryRequestDTO;
import com.xifan.iversion.facade.parameter.response.bo.MessageInfoBO;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/20 0020
 */

public interface MessageServiceClient {

    /**
     * 分页查询 消息列表
     *
     * @param requestDTO
     * @return
     */
    PageResult<MessageInfoBO> queryMessagePageList(MessageQueryRequestDTO requestDTO);
}
