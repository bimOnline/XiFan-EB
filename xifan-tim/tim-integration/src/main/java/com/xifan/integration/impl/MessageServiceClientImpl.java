package com.xifan.integration.impl;

import com.xifan.commons.cores.constant.XifanConstants;
import com.xifan.commons.cores.result.PageResult;
import com.xifan.integration.MessageServiceClient;
import com.xifan.iversion.facade.MessageService;
import com.xifan.iversion.facade.parameter.request.MessageQueryRequestDTO;
import com.xifan.iversion.facade.parameter.response.bo.MessageInfoBO;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/20 0020
 */

@Service
public class MessageServiceClientImpl implements MessageServiceClient {

    @Reference(version = XifanConstants.DUBBO_VERSION_DEFAULT,check = false)
    MessageService messageService;

    @Override
    public PageResult<MessageInfoBO> queryMessagePageList(MessageQueryRequestDTO requestDTO) {
        return messageService.queryMessagePageList(requestDTO);
    }
}
