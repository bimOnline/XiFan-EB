package com.xifan;

import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import com.xifan.commons.util.boot.BootPrintServiceUrlUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * 追忆寻梦
 * rundreams.net
 *
 * @Author:zzh rundreams@yeah.net  @Time:2020/2/12 0012
 */

@Slf4j
@NacosPropertySource(dataId = "xifanTim", autoRefreshed = true)
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class TimApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(TimApplication.class, args);
        BootPrintServiceUrlUtil.printServiceUrl(ctx);
    }
}
