package com.xifan.gateway.dal.entity.auto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 系统API监控 
 * </p>
 *
 * @author 追忆寻梦 Ray Allen
 * rundreams.net
 * rundreams@yeah.net 
 * @since 2020-05-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("system_api_monitor")
public class SystemApiMonitorDO extends Model<SystemApiMonitorDO> {

    private static final long serialVersionUID=1L;

      @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

      /**
     * 访问URL
     */
      private String accessUrl;

      /**
     * 系统名字
     */
      private String systemName;

      /**
     * 是否需要登录
     */
      private String isLogin;

      /**
     * 一分钟最多可以调用多少次
     */
      private Integer callFrequency;

      /**
     * 备注
     */
      private String remark;

      /**
     * 创建时间
     */
      private LocalDateTime createTime;

      /**
     * 修改时间
     */
      private LocalDateTime updateTime;

      /**
     * 0未删除 1 已删除
     */
      private Integer isDelete;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
