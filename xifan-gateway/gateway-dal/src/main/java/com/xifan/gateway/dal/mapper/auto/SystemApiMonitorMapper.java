package com.xifan.gateway.dal.mapper.auto;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xifan.gateway.dal.entity.auto.SystemApiMonitorDO;

/**
 * <p>
 * 系统API监控  Mapper 接口
 * </p>
 *
 * @author 追忆寻梦 Ray Allen
 * rundreams.net
 * rundreams@yeah.net 
 * @since 2020-05-23
 */
public interface SystemApiMonitorMapper extends BaseMapper<SystemApiMonitorDO> {

}
