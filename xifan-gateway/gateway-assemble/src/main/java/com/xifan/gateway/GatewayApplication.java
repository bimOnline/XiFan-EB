package com.xifan.gateway;

import com.xifan.commons.util.boot.BootPrintServiceUrlUtil;
import com.xifan.starter.service.IXifanSystemService;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/13 0013
 */

@Slf4j
@SpringBootApplication
@MapperScan({"com.xifan.gateway.dal.mapper.auto", "com.xifan.iversion.dal.gateway.extra"})
public class GatewayApplication implements ApplicationRunner {

    @Autowired
    IXifanSystemService iXifanSystemService;

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(GatewayApplication.class, args);
        BootPrintServiceUrlUtil.printServiceUrl(ctx);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        iXifanSystemService.printSystemInfo();
    }
}
