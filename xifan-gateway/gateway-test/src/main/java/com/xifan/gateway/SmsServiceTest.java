package com.xifan.gateway;

import cn.hutool.core.date.DateUtil;
import com.xifan.commons.cores.result.AbstractResponseDTO;
import com.xifan.gateway.facade.SmsService;
import com.xifan.gateway.facade.parameter.request.SystemApiMonitorRequestDTO;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/5/23 0023
 */
public class SmsServiceTest extends  BaseTest {

    @Autowired
    SmsService smsService;

    @Test
    public void gatewayInsertDemo(){

        SystemApiMonitorRequestDTO requestDTO =new SystemApiMonitorRequestDTO();
        requestDTO.setAccessUrl(DateUtil.now());
        requestDTO.setSystemName("系统名字");
        requestDTO.setCallFrequency(10);
        requestDTO.setIsLogin("N");
        AbstractResponseDTO abstractResponseDTO = smsService.gatewayInsertDemo(requestDTO);
        logger.info("返回abstractResponseDTO={}", abstractResponseDTO);
    }

}
