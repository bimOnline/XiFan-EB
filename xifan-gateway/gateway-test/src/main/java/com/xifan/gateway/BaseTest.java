package com.xifan.gateway;

import com.xifan.commons.cores.env.Profiles;
import com.xifan.commons.cores.test.CoreTest;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/13 0013
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = com.xifan.gateway.GatewayApplication.class)
@ActiveProfiles(value = Profiles.TEST)
public class BaseTest extends CoreTest {
}
