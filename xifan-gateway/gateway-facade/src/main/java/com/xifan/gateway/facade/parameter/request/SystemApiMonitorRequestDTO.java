package com.xifan.gateway.facade.parameter.request;

import com.xifan.commons.cores.result.AbstractRequestDTO;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 系统API监控
 * </p>
 *
 * @author 追忆寻梦 Ray Allen
 * rundreams.net
 * rundreams@yeah.net
 * @since 2020-05-23
 */
@Getter
@Setter
public class SystemApiMonitorRequestDTO extends AbstractRequestDTO {


    private static final long serialVersionUID = 6208558078064236237L;

    /**
     * 访问URL
     */
    private String accessUrl;

    /**
     * 系统名字
     */
    private String systemName;

    /**
     * 是否需要登录
     */
    private String isLogin;

    /**
     * 一分钟最多可以调用多少次
     */
    private Integer callFrequency;

    /**
     * 备注
     */
    private String remark;


    @Override
    protected boolean isValidGid() {
        return false;
    }

    @Override
    public void valid() {

    }
}
