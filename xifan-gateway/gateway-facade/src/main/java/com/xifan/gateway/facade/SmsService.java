package com.xifan.gateway.facade;


import com.xifan.commons.cores.result.AbstractResponseDTO;
import com.xifan.gateway.facade.parameter.request.SystemApiMonitorRequestDTO;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/20 0020
 */

public interface SmsService {

    AbstractResponseDTO sms();

    /**
     * demo insert by geteway
     *
     * @param requestDTO
     * @return
     */
    AbstractResponseDTO gatewayInsertDemo(SystemApiMonitorRequestDTO requestDTO);

}
