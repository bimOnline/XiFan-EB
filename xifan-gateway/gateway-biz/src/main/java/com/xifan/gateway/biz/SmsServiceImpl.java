package com.xifan.gateway.biz;

import com.xifan.commons.cores.constant.XifanConstants;
import com.xifan.commons.cores.result.AbstractResponseDTO;
import com.xifan.gateway.dal.entity.auto.SystemApiMonitorDO;
import com.xifan.gateway.dal.mapper.auto.SystemApiMonitorMapper;
import com.xifan.gateway.facade.SmsService;
import com.xifan.gateway.facade.parameter.request.SystemApiMonitorRequestDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/13 0013
 */

@Slf4j
@Service(version = XifanConstants.DUBBO_VERSION_DEFAULT)
public class SmsServiceImpl implements SmsService {

    @Autowired
    SystemApiMonitorMapper systemApiMonitorMapper;

    @Override
    public AbstractResponseDTO sms() {
        return null;
    }

    @Override
    public AbstractResponseDTO gatewayInsertDemo(SystemApiMonitorRequestDTO requestDTO) {

        SystemApiMonitorDO monitorDO = new SystemApiMonitorDO();
        BeanUtils.copyProperties(requestDTO, monitorDO);
//        monitorDO.setCreateTime(DateUtil.toLocalDateTime(new Date()));

        systemApiMonitorMapper.insert(monitorDO);

        return AbstractResponseDTO.success();
    }
}
