create table `system_api_monitor` (
	`id` int (11) not null auto_increment,
	`access_url` varchar (256) not null comment '访问URL',
	`system_name` varchar(32) not null comment '系统名字',
	`is_login` varchar(2) not null comment '是否需要登录',
	`call_frequency` int(11) not null comment '一分钟最多可以调用多少次',
	`remark` varchar (1024) null default null comment '备注',
	`create_time` datetime not null comment '创建时间',
	`update_time` timestamp null default current_timestamp on update current_timestamp comment '修改时间',
	`is_delete` int (1) null default '0' comment '0未删除 1 已删除',
	primary key (`id`),
  unique index `url_name_index`(`access_url`,`system_name`),
  index `call_frequency_index`(`call_frequency`)
) comment = '系统API监控 ' collate = 'utf8_general_ci' engine = innodb auto_increment = 0;

