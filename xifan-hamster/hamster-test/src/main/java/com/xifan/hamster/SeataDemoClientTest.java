package com.xifan.hamster;

import com.xifan.hamster.integration.SeataDemoClient;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/5/23 0023
 */
public class SeataDemoClientTest extends BaseTest{

    @Autowired
    SeataDemoClient seataDemoClient;

    @Test
    public void demoInsert(){
        seataDemoClient.demoInsert();
    }

}
