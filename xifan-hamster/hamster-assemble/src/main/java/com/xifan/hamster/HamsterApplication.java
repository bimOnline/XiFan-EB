package com.xifan.hamster;

import com.xifan.commons.util.boot.BootPrintServiceUrlUtil;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/2/13 0013
 */

@Slf4j
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@MapperScan({"com.xifan.hamster.dal.mapper"})
public class HamsterApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(HamsterApplication.class, args);
        BootPrintServiceUrlUtil.printServiceUrl(ctx);
    }
}
