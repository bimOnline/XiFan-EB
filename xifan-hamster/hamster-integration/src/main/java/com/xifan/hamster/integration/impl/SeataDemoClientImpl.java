package com.xifan.hamster.integration.impl;

import cn.hutool.core.date.DateUtil;
import com.xifan.commons.cores.constant.XifanConstants;
import com.xifan.commons.cores.result.AbstractResponseDTO;
import com.xifan.gateway.facade.SmsService;
import com.xifan.gateway.facade.parameter.request.SystemApiMonitorRequestDTO;
import com.xifan.hamster.integration.SeataDemoClient;
import com.xifan.iversion.facade.MessageService;
import com.xifan.iversion.facade.parameter.request.MessageInfoRequestDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/5/23 0023
 */

@Slf4j
@Service("seataDemoClient")
public class SeataDemoClientImpl implements SeataDemoClient {

    @Reference(version = XifanConstants.DUBBO_VERSION_DEFAULT)
    MessageService messageService;

    @Reference(version = XifanConstants.DUBBO_VERSION_DEFAULT)
    SmsService smsService;


    @Override
    public synchronized AbstractResponseDTO demoInsert() {

        MessageInfoRequestDTO requestDTO = new MessageInfoRequestDTO();
        requestDTO.setMsgNo(DateUtil.now());
        requestDTO.setMsgTitle("头部");
        requestDTO.setMsgContent("内容");
        AbstractResponseDTO abstractResponseDTO = messageService.insertDemo(requestDTO);
        log.info("返回abstractResponseDTO={}", abstractResponseDTO);


        SystemApiMonitorRequestDTO requestDTO2 =new SystemApiMonitorRequestDTO();
        requestDTO2.setAccessUrl(DateUtil.now());
        requestDTO2.setSystemName("系统名字");
        requestDTO2.setCallFrequency(10);
        requestDTO2.setIsLogin("N");
        AbstractResponseDTO abstractResponseDTO2 = smsService.gatewayInsertDemo(requestDTO2);
        log.info("返回abstractResponseDTO2={}", abstractResponseDTO2);

        return null;
    }
}
