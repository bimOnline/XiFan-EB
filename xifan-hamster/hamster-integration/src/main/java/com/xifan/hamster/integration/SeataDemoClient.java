package com.xifan.hamster.integration;

import com.xifan.commons.cores.result.AbstractResponseDTO;

/**
 * 追忆寻梦
 * rundreams.net
 * rundreams@yeah.net
 *
 * @Author: Ray Allen  @Time:2020/5/23 0023
 */
public interface SeataDemoClient {

    AbstractResponseDTO demoInsert();
}
