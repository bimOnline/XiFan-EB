## 开发

```bash
# RuoYi-Vue 引用
## 业务支撑系统 采用RuoYi-Vue 前端作为基础
## 源地址：https://gitee.com/y_project/RuoYi-Vue

# 克隆项目
git clone https://gitee.com/rundreams-open/XiFan-EB.git

# 进入项目目录
cd xifan-hamster-ui

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```

浏览器访问 http://localhost:80

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```