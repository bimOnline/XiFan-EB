# XiFan-EB
## 这就是 “稀饭”
![image text](https://compass-product.oss-cn-hangzhou.aliyuncs.com/xf/xifan_334x532.jpg)

### 介绍
XiFan-Enterprise Building 企业分布式系统搭建、SOA服务化、微服务。

### 公司网址
* 官网：http://rundreams.net/
* 博客：http://rundreams.top/

### 软件架构
* 核心框架：SpringBoot + MyBatis-Plus
* 限流：Alibaba Sentinel
* 配置中心：Alibaba Nacos
* 分库分表：Apache Sharding-JDBC
* RPC通信框架：Apache Dubbo
* 分布式协调组件：Apache Zookeeper
* 分布式消息中间件：Apache RocketMQ

### 在线演示
#### Dubbo、Nacos
* dubbo-admin地址：http://xifan.dubbo-admin.rundreams.top/  
* dubbo-admin账户名密码：root   xifan123
* nacos地址：http://xifan.nacos.rundreams.top/nacos/
#### Tim、Tim接口文档
* Tim 默认地址：http://xifan.tim.rundreams.top/  
* Tim 接口文档地址：http://xifan.tim.rundreams.top/doc.html
### Hamster业务支撑系统
* 借鉴 RuoYi-Vue 进行整合：https://gitee.com/y_project/RuoYi-Vue
* 访问地址：http://xifan.hamster.rundreams.top/ 

### 快速启动说明

####  install jar包到本地
* 首先把 xifan-parent install 在本地
* 再把 xifan-commons -> commons-core install 在本地
* 详细配置可参考 xx-assemble 目录下 application-x.yml 配置信息

####  DEV环境 启动
* 资源下载  链接：https://pan.baidu.com/s/1pRrun3sV6DJi5c2RZ1itLQ  提取码：llby
* 数据库sql默认采用公网已有数据库,项目成熟后会取消，自定义sql问题 doc sql v1.0.sql
* 项目依赖 zookeeper dubbo nacos等中间件，本地需启动
* 解压 xifan-source 包，得到 apache-zookeeper-3.5.7-bin   nacos-server-1.2.0 等
* zk启动: xifan-source\apache-zookeeper-3.5.7-bin\apache-zookeeper-3.5.7-bin\bin  双击 zkServer.cmd
* dubbo控制台启动：xifan-source 右键打开cmd 执行 java -jar dubbo-admin-0.1.jar  地址: http://127.0.0.1:8080/ 访问 账户密码均为 root
* dubbo直接运行的jar包，是通过源码打包出来的，也可自行下载源码运行打包 https://github.com/apache/dubbo-admin
* nacos启动：xifan-source\nacos-server-1.2.0\nacos\bin 双击 startup.cmd  地址: http://127.0.0.1:8848/nacos/index.html
* 项目启动：如 xifan-iversion -子模块 iversion-assemble 下 IversionApplication 运行ain方法，多启动几个项目在dubbo控制台查看dubbo服务注册及消费情况

####  PRO环境 启动
* 生产环境自行进行配置

#####
* 工具类：Hutool
* 缓存框架：Redis
* 关系型数据库：Mysql
* 权限框架：Spring Security

### 项目目录说明
##### 1. xifan-commons：核心包
* commons-core：核心工具包，封装统一异常，返回对象，公共类
* commons-mpAutoGenerator: mybatis-plus 自动生成代码

##### 2. xifan-gateway：网关
* 和外部三方公司交互系统，如支付异步通知、公众号管理等
##### 3. xifan-hamster：业务支撑系统
* 用于管理公司内部业务支撑，运营支撑，如会员管理，报表数据统计等
##### 4. xifan-iversion：“艾弗森”，核心底层系统
* 提供与数据库交互的公共接口，根据多个子系统提供相应的facade服务
##### 5. xifan-parent：公共包版本定义
* 定义公司所有包版本，进行统一管理，方便管理依赖统一升级包版本等
##### 6. xifan-tim：“提姆”，业务系统
* 公司业务系统，如公司官网，电商平台，APP服务接口等

### 项目详细目录说明
* xx-assemble：组装层，SpringBoot启动类，日志配置，SpringBoot各个环境配置文件
* xx-biz：业务接口实现
* xx-dal：数据库交互层
* xx-facade：对外提供接口服务
* xx-integration：RPC远程调用外部服务
* xx-test：单元测试模块
* xx-uitl：子项目工具类模块
* xx- api/controller：控制器子模块

### RPC项目调用说明
